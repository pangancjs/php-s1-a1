<?php require "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>
    <?php 
    createProduct("Milk", 100);
    ?>
  </h1>
  <h1>Product List:
    <?php 
    printProducts();
    ?>
  </h1>
  <h1>Product Count:
    <?php 
    countProducts();
    ?>
  </h1>
  <h1>
    <?php 
    deleteProduct();
    ?>
  </h1>
  <h1>Updated Product List:
    <?php 
    printProducts();
    ?>
  </h1>
</body>
</html>